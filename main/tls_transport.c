#include <uxr/client/profile/transport/custom/custom_transport.h>

/* socket includes */
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <netinet/in.h>

/* socket includes */
#include <wolfssl/options.h>
#include <wolfssl/ssl.h>
#include "../certs/wolfssl_buffers.h"


int                sockfd;
struct sockaddr_in servAddr;

/* declares wolfSSL objects */
WOLFSSL_CTX* ctx;
WOLFSSL*     ssl;

struct pollfd pfds;

#define DEFAULT_PORT 8888
#define AGENT_IP "192.168.0.100"

bool ct_tls_open(
        uxrCustomTransport* transport)
{
    bool rv = true;
	int ret = -1;

	int doPeerCheck = 0;

    /* creates a socket that uses an internet IPv4 address */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        printf("[error] failed to create the socket\n");
        rv = false;
    }

    memset(&servAddr, 0, sizeof(servAddr));     /* initializing server address */
    servAddr.sin_family = AF_INET;              /* using IPv4      */
    servAddr.sin_port   = htons(DEFAULT_PORT);  /* on DEFAULT_PORT */

    if (inet_pton(AF_INET, AGENT_IP, &servAddr.sin_addr) != 1) {
        printf("[error] invalid address\n");
        rv =false;
    }

	/* keeps trying to connect to the agent */
	while (ret == -1){
		if ((ret = connect(sockfd, (struct sockaddr*) &servAddr, sizeof(servAddr))) == -1) {
			printf("[error] failed to connect\n");
			rv = false;
			/* slowing down the connection attempts*/
			usleep(1000000);
		}
	}

	if (rv) printf("[log] TCP connection successfully established. \n");

	rv = true; /* if it left the loop above, rv can be set to true */

	if ((ret = wolfSSL_Init()) != WOLFSSL_SUCCESS) {
        printf("[error] failed to initialize the library\n");
        close(sockfd);
		rv = false;
    }

	WOLFSSL_METHOD* method_test = 0;
 
	method_test = wolfSSLv23_client_method();
	if (method_test == NULL) {
		printf("[error] failed to create wolfssl method\n");
		rv = false;
	}

	/* creates and initializes WOLFSSL_CTX */
    if ((ctx = wolfSSL_CTX_new(method_test)) == NULL) {
        printf("[error] failed to create wolfssl_ctx\n");
        ret = -1;
		rv = false;
        close(sockfd);
    }

    /* loads client certificates into WOLFSSL_CTX */ 
     if ((ret = wolfSSL_CTX_load_verify_buffer(ctx, ca_cert_der_2048, sizeof_ca_cert_der_2048, WOLFSSL_FILETYPE_ASN1)) != SSL_SUCCESS) {
        printf("[error] failed to load %d, please check the file.\n",ret);
		rv = false;
    }

    /* not peer check */
    if( doPeerCheck == 0 ){
        wolfSSL_CTX_set_verify(ctx, WOLFSSL_VERIFY_NONE, 0);
    } else {
        WOLFSSL_MSG("Loading... our cert");
        /* load our certificate */
   	    if ((ret = wolfSSL_CTX_use_certificate_chain_buffer_format(ctx, client_cert_der_2048,
            sizeof_client_cert_der_2048, WOLFSSL_FILETYPE_ASN1)) != SSL_SUCCESS) {
            printf("[error] failed to load chain %d, please check the file.\n",ret);
			ret = false;
        }

        if ((ret = wolfSSL_CTX_use_PrivateKey_buffer(ctx, client_key_der_2048,
            sizeof_client_key_der_2048, WOLFSSL_FILETYPE_ASN1))  != SSL_SUCCESS) {
            wolfSSL_CTX_free(ctx); ctx = NULL;
            printf("[error] failed to load key %d, please check the file.\n", ret);
			ret = false;
        }

        wolfSSL_CTX_set_verify(ctx, WOLFSSL_VERIFY_PEER, 0);
    }

    if ((ssl = wolfSSL_new(ctx)) == NULL) {
        printf("[error] failed to create WOLFSSL object\n");
		rv = false;
    }

    if ((ret = wolfSSL_set_fd(ssl, sockfd)) != WOLFSSL_SUCCESS) {
        printf("[error] failed to set the file descriptor\n");
		rv = false;
    }

    if ((ret = wolfSSL_connect(ssl)) != SSL_SUCCESS) {
        printf("[error] failed to connect to wolfSSL\n");
		rv = false;
    }

	if (rv) printf("[log] TLS handshake successfully performed.\n");

    return rv;
}

bool ct_tls_close(
        uxrCustomTransport* transport)
{
    (void) transport;
    close(sockfd);
    printf("[log] transport successfully closed \n");
    return true;
}

size_t ct_tls_write(
        uxrCustomTransport* transport,
        const uint8_t* buf,
        size_t len,
        uint8_t* errcode)
{
    (void) transport;

    size_t rv;
	ssize_t r_written = wolfSSL_write(ssl, buf, len);

	if (r_written < 0) {
        printf("[error] failed to write\n");
        rv = 0;
    } else{
		rv = (size_t) r_written;
	}
    return rv;
}

size_t ct_tls_read(
        uxrCustomTransport* transport,
        uint8_t* buf,
        size_t len,
        int timeout,
        uint8_t* errcode)
{
    (void) transport;

    /* wolfssl max buffer size is int */
	if(len > __INT_MAX__){
		len = __INT_MAX__;
	}

	size_t rv = 0;

	pfds.fd = wolfSSL_get_fd(ssl);
    pfds.events = POLLIN;

    int pending = wolfSSL_pending(ssl);
    int poll_rv = 0;

    if (pending >= len || 0 < (poll_rv = poll(&pfds, 1, timeout)))
    {
        ssize_t r_read = wolfSSL_read(ssl, buf, len);

        if (r_read < 0)
        {
            printf("[error]  failed to read. CODE: %zd, timeout: %d \n", r_read, timeout);
        }
        else
        {
            rv = r_read;
        }
    }
    else
    {
		rv = 0;
    }

    return rv;
}
