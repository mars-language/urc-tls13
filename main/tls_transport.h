#ifndef _my_custom_transport_H_
#define _my_custom_transport_H_

#include <uxr/client/profile/transport/custom/custom_transport.h>

bool ct_tls_open(
        uxrCustomTransport* transport);
bool ct_tls_close(
        uxrCustomTransport* transport);
size_t ct_tls_write(
        uxrCustomTransport* transport,
        const uint8_t* buf,
        size_t len,
        uint8_t* errcode);
size_t ct_tls_read(
        uxrCustomTransport* transport,
        uint8_t* buf,
        size_t len,
        int timeout,
        uint8_t* errcode);

#endif // ifndef _my_custom_transport_H_
