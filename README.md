# micro-ROS Client (TLS 1.3 Transport)

The micro-ROS Client (µRC) is a stripped-down version of a ROS2 node, aiming to provide core DDS functionalities for a microcontroller to participate in a DDS domain similar to a how ROS2 node would.
This repository contains a custom transport based on TLS 1.3 for micro-ROS Clients and Agents to authenticate and encrypt their communication.

Below you can find a step-by-step tutorial on how to deploy this transport on an ESP32, together with a simple example of a micro-ROS Client publishing on an Int32 topic. 

## Installation
To install and run our custom transport, you will need:

* 1x ESP32;
* 1x micro USB cable to connect the ESP32 to your machine;
* WiFi network to connect your nodes;
* and an Ubuntu 20.04 machine with the following software installed on it.

### ROS2
We've used Galactic Geochelone on this project. You can find the step-by-step tutorial on how to install it [here](https://docs.ros.org/en/galactic/Installation/Ubuntu-Development-Setup.html)..

### wolfSSL

This project uses version 5.1.0 of wolfSSL. You can download wolfSSL [here](https://www.wolfssl.com/download/) and build it following the tutorial found [here](https://www.wolfssl.com/docs/wolfssl-manual/ch2/) (use the default configuration).

### Espressif IoT Development Framework (ESP-IDF)
To compile and deploy our code into an ESP32, we will use the ESP-IDF and add custom components from eProsima and wolfSSL. First, let's install ESP-IDF version 4.4:

```bash
mkdir -p ~/esp
cd ~/esp
git clone -b v4.4 --recursive https://github.com/espressif/esp-idf.git
```

#### micro-ROS Component for ESP-IDF
With ESP-IDF correctly installed, you should have a `components` folder located at `~/esp/esp-idf/`. Go ahead and clone the following repository inside it 
```bash
git clone https://github.com/micro-ROS/micro_ros_espidf_component
```

#### wolfSSL Component for ESP-IDF
Again inside the `components` folder, clone the wolfSSL component repository with the command below
```bash
git clone --recursive https://github.com/espressif/esp-wolfssl
```

## Configuring the Environment
With both components installed, you should be able to see folders `micro_ros_espidf_component-galactic` and `esp-wolfssl` inside the `components` folder. Go ahead and clone this repository inside the `examples` folder of the micro-ROS component, located at `esp/esp-idf/components/micro_ros_espidf_component-galactic/examples/` by using the command below

```bash
git clone https://bitbucket.org/mars-language/urc-tls13.git
```

To build and flash your application, use a clean shell environment that is not sourcing the ROS2 setup script and issue the following commands:

```bash
export IDF_PATH=~/esp/esp-idf && . $IDF_PATH/export.sh
cd /urc-tls13
idf.py set-target esp32
idf.py menuconfig
```

When configuring the board, there are three settings to look for: 

* the Agent's IP and PORT, under the menu `micro-ROS Settings`;
* the network's SSID and password, under the menu `micro-ROS Settings -> WiFi Configuration`;
* selecting WLAN as the network interface, under the menu `micro-ROS Settings -> micro-ROS network interface...`;
* and setting wolfSSL as the default ESP-TLS configuration, by checking the option `Include wolfSSL in esp-tls` under `Component config -> wolfSSL`, and by choosing wolfSSL under `Component config -> ESP-TLS-> Choose SSL/TLS...`.

You can now proceed to build and flash the code to your ESP32.

```bash
idf.py build
idf.py flash
```

To monitor its serial output, run:

```bash
idf.py monitor
```

## Known Issues
 
## License
[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)